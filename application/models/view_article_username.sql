CREATE VIEW `article_username`
AS
  SELECT `article`.`id`,
    `title`,
    `alias`,
    `content`,
    `date`,
    `article`.`status`,
    `username` AS `author`,
    `author_id`
  FROM `article` INNER JOIN `login` ON `article`.`author_id` = `login`.`id`;