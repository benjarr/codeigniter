CREATE TABLE IF NOT EXISTS `article` (
  `id` int(11) NOT NULL,
  `title` varchar(64) NOT NULL,
  `alias` varchar(64) NOT NULL,
  `content` text NOT NULL,
  `date` datetime NOT NULL,
  `status` char(1) NOT NULL DEFAULT'W',
  `author_id` int(11) NOT NULL
) ENGINE = InnoDB DEFAULT CHARSET = utf8;

ALTER TABLE `article` ADD PRIMARY KEY(`id`);
ALTER TABLE `article` ADD KEY `idx_author_id` (`author_id`);

ALTER TABLE `article` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `article` ADD CONSTRAINT `fk_article_login_id` FOREIGN KEY(`author_id`) REFERENCES `login` (`id`);
